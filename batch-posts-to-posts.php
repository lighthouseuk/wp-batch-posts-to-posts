<?php
/**
 * Plugin Name: Batch Posts-2-Posts connections
 * Description: Easily create P2P relations
 * Requires Plugins: posts-to-posts
 * Version: 1.0
 * Author: Mike Manger
 * Author URI: https://lighthouseuk.net/
 * Update URI: https://bitbucket.org/lighthouseuk/wp-batch-posts-to-posts/
 *
 * @package lh\bp2p
 */

if ( ! is_admin() ) {
	return;
}

/**
 * Adds admin menu item to tools section.
 */
function bp2p_add_menu(): void {
	add_submenu_page( 'tools.php', __( 'Batch Posts-2-Posts', 'bp2p' ), __( 'Batch Posts-2-Posts', 'bp2p' ), 'manage_options', 'bp2p_tools_page', 'bp2p_print_tools_page' );
}
add_action( 'admin_menu', 'bp2p_add_menu' );

/**
 * Output tools page.
 */
function bp2p_print_tools_page(): void {
	if ( ! class_exists( 'P2P_Connection_Type_Factory' ) ) {
		wp_die( esc_html__( 'Please enable P2P plugin', 'bp2p' ) );
	}
	if ( isset( $_GET['step'] ) ) {
		$step = (int) $_GET['step'];
		if ( ! wp_verify_nonce( $_GET['bp2p_wpnonce'], 'step' ) ) {
			wp_die( esc_html__( 'Security - nonce not setup', 'bp2p' ) );
		}
	} else {
		$step = 1;
	}

	$last_step = $step - 1;

	$action_url = add_query_arg( array( 'step' => $step + 1 ) );
	?>
		<div class="wrap">
			<h1><?php esc_html_e( 'Batch Posts-2-Posts', 'bp2p' ); ?></h1>
			<form method="get" action="<?php echo esc_url( $action_url ); ?>">
				<?php // translators: %d: Current step. ?>
				<h2><?php echo esc_html( sprintf( __( 'Step %d', 'bp2p' ), $step ) ); ?></h2>
				<input type="hidden" name="page" value="bp2p_tools_page" />
				<input type="hidden" name="step" value="<?php echo $step + 1; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>" />
				<?php if ( isset( $_GET['instance'] ) ) : ?>
					<input type="hidden" name="instance" value="<?php echo esc_attr( $_GET['instance'] ); ?>" />
				<?php endif; ?>
				<?php if ( isset( $_GET['direction'] ) ) : ?>
					<input type="hidden" name="direction" value="<?php echo esc_attr( $_GET['direction'] ); ?>" />
				<?php endif; ?>
				<?php if ( isset( $_GET['taxonomy'] ) ) : ?>
					<input type="hidden" name="taxonomy" value="<?php echo esc_attr( $_GET['taxonomy'] ); ?>" />
				<?php endif; ?>
				<?php if ( isset( $_GET['post_id'] ) ) : ?>
					<input type="hidden" name="post_id" value="<?php echo esc_attr( $_GET['post_id'] ); ?>" />
				<?php endif; ?>
				<?php if ( isset( $_GET['term_id'] ) ) : ?>
					<input type="hidden" name="term_id" value="<?php echo esc_attr( $_GET['term_id'] ); ?>" />
				<?php endif; ?>

				<?php wp_nonce_field( 'step', 'bp2p_wpnonce', false ); ?>

				<table class="form-table">
					<tbody>
						<?php if ( 1 === $step ) : ?>
							<tr>
								<th scope="row">
									<label for="bp2p_instance_name">Connection</label>
								</th>
								<td>
									<?php
									$instances = P2P_Connection_Type_Factory::get_all_instances();

									if ( isset( $_GET['instance'] ) ) {
										$current_instance = $_GET['instance'];
									} else {
										$current_instance = '';
									}

									echo '<select id="bp2p_instance_name" name="instance">';

									foreach ( $instances as $p2p_type => $ctype ) {
										echo '<option value="' . esc_attr( $p2p_type ) . '" ' . selected( $p2p_type, $current_instance, false ) . '>' . esc_html( $ctype->get_desc() ) . '</option>';
									}
									echo '</select>';
									?>
								</td>
							</tr>
						<?php elseif ( 2 === $step ) : ?>
							<?php
							// Pick which post type we want to copy our post from.

							$instance       = P2P_Connection_Type_Factory::get_instance( $_GET['instance'] );
							$from_post_type = get_post_type_object( $instance->side['from']->query_vars['post_type'][0] );
							$to_post_type   = get_post_type_object( $instance->side['to']->query_vars['post_type'][0] );

							if ( isset( $_GET['direction'] ) ) {
								$current_direction = $_GET['direction'];
							} else {
								$current_direction = '';
							}
							?>
							<tr>
								<th>
									<label for="bp2p_direction"><?php esc_html_e( 'Choose direction', 'bp2p' ); ?></label>
								</th>
								<td>
									<?php echo esc_html( $from_post_type->label ); ?>
									<select id="bp2p_direction" name="direction" value="direction">
										<option value="post_to_taxonomy"><?php esc_html_e( 'Post &#8594; Taxonomy', 'bp2p' ); ?></option>
										<option value="taxonomy_to_post" <?php selected( 'taxonomy_to_post', $current_direction ); ?>><?php esc_html_e( 'Taxonomy &#8592; Post', 'bp2p' ); ?></option>
									</select>
									<?php echo esc_html( $to_post_type->label ); ?>
									<p class="description"><?php esc_html_e( 'Choose which post type ', 'bp2p' ); ?></p>
								</td>
							</tr>
						<?php elseif ( 3 === $step ) : ?>
							<?php
							// Pick a taxonomy.

							$instance = P2P_Connection_Type_Factory::get_instance( $_GET['instance'] );
							if ( 'post_to_taxonomy' === $_GET['direction'] ) {
								$taxonomy_post_type = get_post_type_object( $instance->side['to']->query_vars['post_type'][0] );
							} else {
								$taxonomy_post_type = get_post_type_object( $instance->side['from']->query_vars['post_type'][0] );
							}

							if ( isset( $_GET['taxonomy'] ) ) {
								$current = $_GET['taxonomy'];
							} else {
								$current = '';
							}
							?>
							<tr>
								<th>
									<?php // translators: %s: Post type label. ?>
									<label for="bp2p_taxonomy_name"><?php echo esc_html( sprintf( __( 'Choose a %s taxonomy', 'bp2p' ), $taxonomy_post_type->label ) ); ?></label>
								</th>
								<td>
									<?php
										$taxonomies = get_object_taxonomies( $taxonomy_post_type->name, 'objects' );
									?>
									<select id="bp2p_taxonomy_name" name="taxonomy">
										<?php
										foreach ( $taxonomies as $taxonomy ) {
											echo '<option value="' . esc_attr( $taxonomy->name ) . '" ' . selected( $taxonomy->name, $current, false ) . '>' . esc_html( $taxonomy->label ) . '</option>';
										}
										?>
									</select>
								</td>
							</tr>
						<?php elseif ( 4 === $step ) : ?>
							<?php
							// Let the user select which post they want and which term they want to relate it to.

							$instance = P2P_Connection_Type_Factory::get_instance( $_GET['instance'] );
							if ( 'post_to_taxonomy' === $_GET['direction'] ) {
								$posts_post_type    = get_post_type_object( $instance->side['from']->query_vars['post_type'][0] );
								$taxonomy_post_type = get_post_type_object( $instance->side['to']->query_vars['post_type'][0] );
							} else {
								$posts_post_type    = get_post_type_object( $instance->side['to']->query_vars['post_type'][0] );
								$taxonomy_post_type = get_post_type_object( $instance->side['from']->query_vars['post_type'][0] );
							}

							if ( isset( $_GET['post_id'] ) ) {
								$current = $_GET['post_id'];
							} else {
								$current = 0;
							}

							$args  = array(
								'post_type' => $posts_post_type->name,
							);
							$posts = get_posts( $args );
							?>
							<tr>
								<th>
									<?php // translators: %s: Post type singular label. ?>
									<label for="bp2p_post_id"><?php echo esc_html( sprintf( __( 'Choose a %s post', 'bp2p' ), $posts_post_type->labels->singular_name ) ); ?></label>
								</th>
								<td>
									<?php
										echo '<select id="bp2p_post_id" name="post_id">';
									foreach ( $posts as $post ) {
										echo '<option value="' . esc_attr( $post->ID ) . '" ' . selected( $post->ID, $current, false ) . '>' . esc_html( $post->post_title ) . '</option>';
									}
										echo '</select>';
									?>
								</td>
							</tr>
							<tr>
								<th>
									<?php // translators: %s: Post type label. ?>
									<label for="bp2p_term_id"><?php echo esc_html( sprintf( __( 'Choose a %s term', 'bp2p' ), $taxonomy_post_type->labels->singular_name ) ); ?></label>
								</th>
								<td>
									<?php
									if ( isset( $_GET['term_id'] ) ) {
										$current = $_GET['term_id'];
									} else {
										$current = 0;
									}

										$args = array(
											'taxonomy' => $_GET['taxonomy'],
											'name'     => 'term_id',
											'id'       => 'bp2p_term_id',
											'selected' => $current,
										);
										wp_dropdown_categories( $args );
										?>
								</td>
							</tr>
						<?php elseif ( 5 === $step ) : ?>
							<?php
							// Preview the new connections to the user.

							$post_id     = (int) $_GET['post_id'];
							$target_post = get_post( $post_id );
							$taxonomy    = $_GET['taxonomy'];
							$term_id     = (int) $_GET['term_id'];
							$target_term = get_term( $term_id, $taxonomy );

							if ( is_null( $target_post ) || is_wp_error( $target_term ) ) {
								wp_die( esc_html__( 'Error getting post or term', 'bp2p' ) );
							}

							$args      = array(
								'tax_query' => array(
									array(
										'taxonomy' => $taxonomy,
										'field'    => 'id',
										'terms'    => $term_id,
									),
								),
							);
							$the_query = new WP_Query( $args );
							?>
							<tr>
								<th>
									<label><?php esc_html_e( 'Preview', 'bp2p' ); ?></label>
								</th>
								<td>
									<?php
									// translators: %1$s: Post title %2$s: Target term name.
									echo esc_html( sprintf( __( 'Set %1$s as related to the following posts in %2$s', 'bp2p' ), $target_post->post_title, $target_term->name ) );

									if ( $the_query->have_posts() ) {
										echo '<ul>';
										while ( $the_query->have_posts() ) {
											$the_query->the_post();
											echo '<li>';
											the_title();
											echo '</li>';
										}
										echo '</ul>';
									} else {
										echo '<p>' . esc_html__( 'No posts in category!', 'bp2p' ) . '</p>';
									}
									wp_reset_postdata();
									?>
								</td>
							</tr>
						<?php elseif ( 6 === $step ) : ?>
							<?php
							// Lets finally run the import.
							$post_id     = (int) $_GET['post_id'];
							$target_post = get_post( $post_id );
							$taxonomy    = $_GET['taxonomy'];
							$term_id     = (int) $_GET['term_id'];
							$target_term = get_term( $term_id, $taxonomy );

							if ( is_null( $target_post ) || is_wp_error( $target_term ) ) {
								wp_die( esc_html__( 'Error getting post or term', 'bp2p' ) );
							}

							$p2p_type_instance = p2p_type( $_GET['instance'] );
							if ( false === $p2p_type_instance ) {
								wp_die( esc_html__( 'Error getting P2P instance', 'bp2p' ) );
							}

							$args      = array(
								'tax_query' => array(
									array(
										'taxonomy' => $taxonomy,
										'field'    => 'id',
										'terms'    => $term_id,
									),
								),
							);
							$the_query = new WP_Query( $args );
							?>
							<tr>
								<th>
									<?php esc_html_e( 'Results', 'bp2p' ); ?>
								</th>
								<td>
									<?php
									if ( $the_query->have_posts() ) {
										echo '<ul>';
										// translators: %s: The page title.
										$error_message = __( '%s&hellip; error', 'bp2p' );
										// translators: %s: The page title.
										$duplicate_message = __( '%s&hellip; duplicate connection', 'bp2p' );
										// translators: %s: The page title.
										$done_message = __( '%s&hellip; done', 'bp2p' );
										while ( $the_query->have_posts() ) {
											$the_query->the_post();
											$make_connection = $p2p_type_instance->connect(
												$post_id,
												get_the_ID(),
												array(
													'date' => current_time( 'mysql' ),
												)
											);
											echo '<li>';
											if ( is_wp_error( $make_connection ) ) {
												if ( in_array( 'duplicate_connection', $make_connection->get_error_codes(), true ) ) {
													printf( esc_html( $duplicate_message ), esc_html( get_the_title() ) );
												} else {
													printf( esc_html( $error_message ), esc_html( get_the_title() ) );
												}
											} else {
												printf( esc_html( $done_message ), esc_html( get_the_title() ) );
											}
											echo '</li>';
										}
										echo '</ul>';
										esc_html_e( 'All Done!', 'bp2p' );
									} else {
										echo '<p>' . esc_html__( 'No posts in category!', 'bp2p' ) . '</p>';
									}
									wp_reset_postdata();
									?>
								</td>
							</tr>
						<?php endif; ?>
					</tbody>
				</table>
				<p>
					<?php
					if ( $step > 1 ) :
						$args      = array(
							'step' => $last_step,
						);
						$back_link = add_query_arg( $args );
						?>
						<a href="<?php echo esc_url( $back_link ); ?>" class="button"><?php esc_html_e( 'Back', 'bp2p' ); ?></a>
					<?php endif; ?>
					<?php
					if ( 5 === $step ) {
						submit_button( __( 'Submit', 'bp2p' ), 'primary', 'submit', false );
					} elseif ( $step < 6 ) {
						submit_button( __( 'Next', 'bp2p' ), 'primary', 'submit', false );
					}
					?>
				</p>
			</form>
		</div>
	<?php
}
