=== Batch Posts 2 Posts ===
Contributors: mikemanger
Tags: connections, admin
Requires at least: 3.1.0
Tested up to: 6.6
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Easily create Posts 2 Posts relations.

== Description ==

This plugin makes it easy to create relations using the [Posts 2 Posts](https://wordpress.org/plugins/posts-to-posts/)
plugin.

For example you create a new "getting-started.pdf" post is your Downloads custom post type.
This plugin will let you assign this new post all posts in the "Getting started" post category.

The use cases are limited but it saves you (and your clients) from having to manually go into
and edit potentially hundreds of posts.

== Installation ==

1. Install and setup the [Posts 2 Posts](https://wordpress.org/plugins/posts-to-posts/) plugin
2. Upload `batch-posts-to-posts` to the `/wp-content/plugins/` directory
3. Activate the plugin through the 'Plugins' menu in WordPress
4. Access the plugin wizard through the 'Batch Posts-2-Posts' in the 'Tools' menu in WordPress
